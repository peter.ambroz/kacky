<?php

namespace App\Kacky;

use Exception;
use mysqli;
use mysqli_result;

class MySQL extends mysqli
{
    function __construct($h, $u, $p, $d)
    {
        parent::__construct($h, $u, $p, $d);
        $this->set_charset('utf8');
    }

    private function mkRefs(array $data): array
    {
        $refs = [];
        foreach ($data as $k => $v) {
            $refs[$k] = &$data[$k];
        }

        return $refs;
    }

    /**
     * @throws Exception
     */
    public function q(string $q, ?array $bind_vars = null): mysqli_result|bool
    {
        if ($bind_vars !== null) {
            $stmt = $this->stmt_init();
            if (!$stmt->prepare($q)) {
                throw new Exception('SQL prepare error: ' . $stmt->error . ' [' . $q . ']');
            }

            if (count($bind_vars)) {
                if (!call_user_func_array([$stmt, 'bind_param'], $this->mkRefs($bind_vars))) {
                    $stmt->close();
                    throw new Exception('SQL bind_param error: ' . $stmt->error . ' [' . $q . '] [' . print_r($bind_vars, true) . ']');
                }
            }

            if (!$stmt->execute()) {
                throw new Exception('SQL execute error: ' . $stmt->error . ' [' . $q . '] [' . print_r($bind_vars, true) . ']');
            }

            $res = $stmt->get_result();
            $stmt->close();

            if ($res === false) {
                $res = true;
            }
        } else {
            $res = $this->query($q);
        }

        if ($res === false) {
            throw new Exception('SQL query error: ' . $this->error . ' [' . $q . ']');
        } else {
            return $res;
        }
    }

    /**
     * @throws Exception
     */
    public function getrow(string $q, ?array $bind_vars = null): array|bool|null
    {
        $res = $this->q($q, $bind_vars);

        if ($res->num_rows > 0) {
            $row = $res->fetch_assoc();
        } else {
            $row = null;
        }
        $res->close();

        return $row;
    }
}
