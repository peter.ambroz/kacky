<?php

declare(strict_types=1);

namespace App\Kacky;

class PlayRequest
{
    public function __construct(
        public readonly int $playerId,
        public readonly int $cardPos,
        public readonly int $duck0,
        public readonly ?int $duck1,
        public readonly ?array $permutation,
    ) {}
}
