<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\Enum\Color;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class DvojitaTrefa extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Dvojitá trefa';
        $this->description = 'Zastřelte dvě zaměřené kachny v řadě za sebou. Karty zaměřovačů z těchto polí odstraňte.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::DVOJITA_TREFA;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $duck0 = $request->duck0;
        $killedDucks = [];

        $targetDuck0 = $table->getDuckOnBoard($duck0);
        $targetDuck1 = $table->getDuckOnBoard($duck0 + 1);

        if ($targetDuck1->getColor() !== Color::WATER) {
            $color = $table->removeDuck($duck0 + 1, true);

            if ($color !== Color::WATER) {
                $killedDucks[] = $duck0 + 1;
            }
        }

        if ($targetDuck0->getColor() !== Color::WATER) {
            $color = $table->removeDuck($duck0, true);

            if ($color !== Color::WATER) {
                $killedDucks[] = $duck0;
            }
        }

        $table->resetTarget($duck0 + 1);
        $table->resetTarget($duck0);

        $table->refillDucksOnBoard();

        return new PlayResponse([$duck0, $duck0 + 1], $killedDucks);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        // Checks if the duck is not last in the row
        if ($request->duck0 == Table::VISIBLE_DUCKS - 1) {
            return false;
        }
        // Checks if selected duck and one on the right is not targeted
        if (!$table->isTargeted($request->duck0) || !$table->isTargeted($request->duck0 + 1)) {
            return false;
        }

        return true;
    }
}
