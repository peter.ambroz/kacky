<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class KachniTanec extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Kachní tanec';
        $this->description = 'Zamíchejte všechny karty v řadě do balíčku kachen a vyložte zleva doprava nových šest karet do řady.';
        $this->paramClass = CardClass::ZERO;
        $this->type = CardType::KACHNI_TANEC;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $table->reshuffleDucks();

        return new PlayResponse();
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        return true;
    }
}
