<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\Enum\Color;
use App\Kacky\Enum\DuckState;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class ZivyStit extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Živý štít';
        $this->description = 'Skryjte svoji kachnu pod kachnu soupeře, která je v řadě přímo před ní nebo za ní.';
        $this->paramClass = CardClass::DOUBLE;
        $this->type = CardType::ZIVY_STIT;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $table->protectDuckWithDuck($request->duck0, $request->duck1);

        return new PlayResponse([min($request->duck0, $request->duck1)]);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        $myColor = $request->myColor;
        $duck0 = $request->duck0;
        $duck1 = $request->duck1;

        if (!$table->isOwn($myColor, $duck0)) {
            return false;
        }

        $duckOnBoard = $table->getDuckOnBoard($duck0);

        if ($duckOnBoard->getProtectionStatus() !== DuckState::DUCK_ONLY) {
            return false;
        }

        if ($duck1 === null) {
            // If selected first, check right neighbour if WATER or same color or duck has duck on it
            if ($duck0 === 0) {
                if ($table->isOwn(Color::WATER, $duck0 + 1)
                    || $table->isOwn($myColor, $duck0 + 1)
                    || self::isDuckOnDuck($table, $duck0 + 1))
                {
                    return false;
                }
                // If selected last, check left neighbour if WATER or same color or duck has duck on it
            } elseif ($duck0 === Table::VISIBLE_DUCKS - 1) {
                if ($table->isOwn(Color::WATER, $duck0 - 1)
                    || $table->isOwn($myColor, $duck0 - 1)
                    || self::isDuckOnDuck($table, $duck0 - 1))
                {
                    return false;
                }
                // If selected middle, check right and left neighbour if WATER or same color or duck has duck on it
            } else {
                if (($table->isOwn(Color::WATER, $duck0 - 1)
                        || $table->isOwn($myColor, $duck0 - 1)
                        || self::isDuckOnDuck($table, $duck0 - 1))
                    && ($table->isOwn(Color::WATER, $duck0 + 1)
                        || $table->isOwn($myColor, $duck0 + 1)
                        || self::isDuckOnDuck($table, $duck0 + 1)))
                {
                    return false;
                }
            }
        } elseif (abs($duck0 - $duck1) !== 1
            || $table->isOwn($myColor, $duck1)
            || $table->isOwn(Color::WATER, $duck1))
        {
            return false;
        } elseif (self::isDuckOnDuck($table, $duck1)) {
            return false;
        }

        return true;
    }

    private static function isDuckOnDuck(Table $table, int $index): bool
    {
        $var = $table->getDuckOnBoard($index)->getProtectionStatus();

        return in_array($var, [DuckState::DUCK_ON_DUCK, DuckState::DUCK_ON_DUCK_PROTECTED], true);
    }
}
