<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\Enum\Color;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class JejdaVedle extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Jejda vedle';
        $this->description = 'Zastřelte libovolnou kachnu v řadě před nebo za zaměřeným polem. Kartu zaměřovače z tohoto pole odstraňte.';
        $this->paramClass = CardClass::DOUBLE;
        $this->type = CardType::JEJDA_VEDLE;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $duck1 = $request->duck1;
        $auxDuck = $table->getDuckOnBoard($duck1);
        $killedDucks = [];

        if ($auxDuck->getColor() !== Color::WATER) {
            $color = $table->removeDuck($duck1, true);

            if ($color !== Color::WATER) {
                $killedDucks[] = $duck1;
            }
        }

        $table->resetTarget($request->duck0);
        $table->refillDucksOnBoard();

        return new PlayResponse([$duck1], $killedDucks);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        if ($request->duck1 === null) {
            if (!$table->isTargeted($request->duck0)) {
                return false;
            }
        } else {
            if (abs($request->duck0 - $request->duck1) !== 1) {
                return false;
            }
        }

        return true;
    }
}
