<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class Rosambo extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Rošambo';
        $this->description = 'Změňte libovolně pozice všech šesti karet v řadě.';
        $this->paramClass = CardClass::SPECIAL;
        $this->type = CardType::ROSAMBO;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $table->reorderDucks($request->permutation);

        return new PlayResponse();
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        return true;
    }
}
