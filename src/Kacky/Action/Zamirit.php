<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class Zamirit extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Zamířit';
        $this->description = 'Umístěte kartu zaměřovače nad jednu libovolnou kartu oblohy.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::ZAMIRIT;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $table->setTarget($request->duck0);

        return new PlayResponse([$request->duck0]);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        return !$table->isTargeted($request->duck0);
    }
}
