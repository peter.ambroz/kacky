<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class Leharo extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Leháro';
        $this->description = 'Vyměňte v řadě jednu svoji kachnu s kartou přímo za ní.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::LEHARO;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $table->swapDucks($request->duck0, $request->duck0 + 1);

        return new PlayResponse([$request->duck0]);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        // Checks if selected duck has not the player's color nor right neighbour
        if ($request->duck0 === Table::VISIBLE_DUCKS - 1) {
            return false;
        }

        if (!$table->isOwn($request->myColor, $request->duck0)) {
            return false;
        }

        return true;
    }
}
