<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\Enum\Color;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class Vystrelit extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Vystřelit';
        $this->description = 'Zastřelte jednu zaměřenou kachnu. Kartu zaměřovače z tohoto pole odstraňte.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::VYSTRELIT;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $duck0 = $request->duck0;
        $targetDuck = $table->getDuckOnBoard($duck0);

        $killedDucks = [];

        if ($targetDuck->getColor() !== Color::WATER) {
            $color = $table->removeDuck($duck0, true);

            if ($color !== Color::WATER) {
                $killedDucks[] = $duck0;
            }
        }
        $table->resetTarget($duck0);
        $table->refillDucksOnBoard();

        return new PlayResponse([$duck0], $killedDucks);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        return $table->isTargeted($request->duck0);
    }
}
