<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class StrilejVlevo extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Střílej vlevo';
        $this->description = 'Posuňte libovolnou kartu zaměřovače o jedno pole doleva.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::STRILEJ_VLEVO;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $table->resetTarget($request->duck0);
        $table->setTarget($request->duck0 - 1);

        return new PlayResponse([$request->duck0]);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        // Checks if selected duck is not tageted or has left neighbour targeted
        if ($request->duck0 === 0) {
            return false;
        }

        if ($table->isTargeted($request->duck0 - 1) || !$table->isTargeted($request->duck0)) {
            return false;
        }

        return true;
    }
}
