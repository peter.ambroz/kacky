<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class DvojitaHrozba extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Dvojitá hrozba';
        $this->description = 'Umístěte dvě karty zaměřovačů nad dvě sousedící pole v řadě.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::DVOJITA_HROZBA;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $duck0 = $request->duck0;

        $table->setTarget($duck0);
        $table->setTarget($duck0 + 1);

        return new PlayResponse([$duck0, $duck0 + 1]);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        // Checks if the duck is not last in the row
        if ($request->duck0 === Table::VISIBLE_DUCKS - 1) {
            return false;
        }

        // Checks if either selected duck or the one on the right is targeted
        if ($table->isTargeted($request->duck0) || $table->isTargeted($request->duck0 + 1)) {
            return false;
        }

        return true;
    }
}
