<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\Enum\Color;
use App\Kacky\Enum\DuckState;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;
use App\Kacky\Water;

class KachniUnik extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Kachní únik';
        $this->description = 'Zakryjte jednu libovolnou kachnu v řadě na jedno kolo hry touto kartou.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::KACHNI_UNIK;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $table->protectDuckWithAction(
            $table->getDuckOnBoard($request->duck0),
            $table->removeCard($request->playerId, $request->cardPos),
        );

        $table->addCard($request->playerId);

        return new PlayResponse([$request->duck0]);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        $duck0 = $table->getDuckOnBoard($request->duck0);

        if ($duck0 instanceof Water) {
            return false;
        }

        if ($duck0->getProtectionStatus() === DuckState::DUCK_PROTECTED) {
            return false;
        }

        return true;
    }
}
