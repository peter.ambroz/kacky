<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class Turbokachna extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Turbokachna';
        $this->description = 'Posuňte jednu svoji kachnu na pole na začátku řady.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::TURBOKACHNA;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        for ($i = $request->duck0; $i > 0; $i--) {
            $table->swapDucks($i, $i - 1);
        }

        return new PlayResponse([$request->duck0]);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        // Checks if selected duck is the same color as player
        return $table->isOwn($request->myColor, $request->duck0);
    }
}
