<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class KachniPochod extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Kachní pochod';
        $this->description = 'Posuňte všechny karty v řadě o jedno pole dopředu. První kartu v řadě vraťte zpět do balíčku kachen.';
        $this->paramClass = CardClass::ZERO;
        $this->type = CardType::KACHNI_POCHOD;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        for ($i = 0; $i < Table::VISIBLE_DUCKS - 1; $i++) {
            $table->swapDucks($i, $i + 1);
        }

        $table->removeDuck(Table::VISIBLE_DUCKS - 1, false);
        $table->refillDucksOnBoard();

        return new PlayResponse();
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        return true;
    }
}
