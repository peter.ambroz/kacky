<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\Enum\Color;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class DivokejBill extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Divokej Bill';
        $this->description = 'Zastřelte libovolnou kachnu v řadě bez ohledu na to, zda je zaměřená.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::DIVOKEJ_BILL;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $duck0 = $request->duck0;
        $targetDuck = $table->getDuckOnBoard($duck0);

        $killedDucks = [];

        if ($targetDuck->getColor() !== Color::WATER) {
            $color = $table->removeDuck($duck0, true);

            if ($color !== Color::WATER) {
                $killedDucks[] = $duck0;
            }
        }
        $table->resetTarget($duck0);
        $table->refillDucksOnBoard();

        return new PlayResponse([$duck0], $killedDucks);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        return true;
    }
}
