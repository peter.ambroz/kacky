<?php

declare(strict_types=1);

namespace App\Kacky\Action;

use App\Kacky\ActionCard;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\PlayRequest;
use App\Kacky\PlayResponse;
use App\Kacky\Table;
use App\Kacky\ValidateRequest;

class Chvatam extends ActionCard
{
    public function __construct()
    {
        $this->name = 'Chvátám';
        $this->description = 'Vyměňte v řadě jednu svoji kachnu s kartou přímo před ní.';
        $this->paramClass = CardClass::SINGLE;
        $this->type = CardType::CHVATAM;
    }

    public function play(Table $table, PlayRequest $request): PlayResponse
    {
        $table->swapDucks($request->duck0, $request->duck0 - 1);

        return new PlayResponse([$request->duck0]);
    }

    public function validate(Table $table, ValidateRequest $request): bool
    {
        // Checks if selected duck has not the player's color nor right neighbour
        if ($request->duck0 === 0) {
            return false;
        }

        if (!$table->isOwn($request->myColor, $request->duck0)) {
            return false;
        }

        return true;
    }
}
