<?php

declare(strict_types=1);

namespace App\Kacky;

/**
 * Stack and Queue functionality
 */
class Stack
{
    private array $items = [];

    public function isEmpty(): bool
    {
        return count($this->items) === 0;
    }

    /**
     * QUEUE: removes the object at the bottom and returns it
     */
    public function get()
    {
        return array_shift($this->items);
    }

    /**
     * STACK: removes the object at the top and returns it
     */
    public function pop()
    {
        return array_pop($this->items);
    }

    /**
     * STACK: get the object at the top, return it but do not remove it
     */
    public function peek()
    {
        if ($this->isEmpty()) {
            return null;
        } else {
            return $this->items[count($this->items)-1];
        }
    }

    /**
     * QUEUE: adds item to the top
     */
    public function add($item): void
    {
        $this->items[] = $item;
    }

    /**
     * STACK: pushes an item onto the top
     */
    public function push($item): void
    {
        $this->items[] = $item;
    }

    public function shuffle(): array
    {
        shuffle($this->items);

        return $this->items;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    public function __toString(): string
    {
        return sprintf("Stack: [items:\n%s]\n", implode("", $this->items));
    }
}
