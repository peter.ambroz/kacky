<?php

declare(strict_types=1);

namespace App\Kacky;

use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\Enum\Color;

abstract class ActionCard
{
    protected string $name;

    protected string $description;

    protected CardClass $paramClass;

    protected CardType $type;

    abstract public function play(Table $table, PlayRequest $request): PlayResponse;

    abstract public function validate(Table $table, ValidateRequest $request): bool;

    public function getType(): CardType
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getClass(): CardClass
    {
        return $this->paramClass;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function __toString(): string
    {
        return sprintf("ActionCard: [name: %s]\n", $this->name);
    }
}
