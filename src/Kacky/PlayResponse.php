<?php

declare(strict_types=1);

namespace App\Kacky;

class PlayResponse
{
    /**
     * @param int[] $affectedDucks
     * @param int[] $killedDucks
     */
    public function __construct(public readonly array $affectedDucks = [], public readonly array $killedDucks = [])
    {
    }
}
