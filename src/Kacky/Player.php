<?php

declare(strict_types=1);

namespace App\Kacky;

use App\GameManager\ObjectWithId;
use App\Kacky\Enum\Color;

class Player implements ObjectWithId
{
    public const LIVES = 5;

    private ?Color $color = null;

    private int $lives;

    function __construct(private readonly int $id, private readonly string $name)
    {
        $this->reset();
    }

    public function reset(): void
    {
        $this->lives = self::LIVES;
    }

    public function getColor(): ?Color
    {
        return $this->color;
    }

    public function setColor(?Color $color): void
    {
        $this->color = $color;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLives(): int
    {
        return $this->lives;
    }

    public function decreaseLives(): void
    {
        $this->lives--;
    }

    public function __toString(): string
    {
        return sprintf("Player: [id: %d, name: %s, color: %d, lives: %d]\n",
            $this->id,
            $this->name,
            $this->color->value,
            $this->lives,
        );
    }
}
