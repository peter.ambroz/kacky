<?php

declare(strict_types=1);

namespace App\Kacky\Model;

use App\Kacky\DB;
use App\GameManager\Connection;
use App\GameManager\ObjectWithId;
use Exception;

class User implements ObjectWithId
{
    private ?int $id = null;

    private string $name;

    private string $passhash;

    private ?int $game_id;

    private ?string $foreign_id;

    public function __construct(private ?Connection $socket = null)
    {
        $this->game_id = null;
        $this->foreign_id = null;
    }

    public function __destruct()
    {
        $this->socket = null;
    }

    /**
     * @throws Exception
     */
    public function verifyFromDB(string $username, string $password): void
    {
        $this->loadFromDB(null, $username);

        if (($this->getId() === null) || !$this->verifyPassword($password)) {
            throw new Exception('Invalid user or password');
        }
    }

    /**
     * @throws Exception
     */
    public function verifyFacebook(string $token): void
    {
        $data = file_get_contents('https://graph.facebook.com/me?access_token='.$token);
        $user_data = json_decode($data, true);

        if (($user_data === null) || !array_key_exists('id', $user_data)) {
            throw new Exception('Invalid Facebook access token');
        }

        $this->loadFromDB(null, null, 'F'.$user_data['id']);

        if ($this->getId() === null) {
            $username = explode(' ', $user_data['name'], 2);
            $this->name = $username[0];
            $this->foreign_id = 'F'.$user_data['id'];
            $this->passhash = '!';
            $this->saveToDB();
        }
    }

    /**
     * @throws Exception
     */
    public function verifyGoogle(string $token): void
    {
        $data = file_get_contents('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='.$token);
        $user_data = json_decode($data, true);

        if (($user_data === null) || !array_key_exists('sub', $user_data)) {
            throw new Exception('Invalid Google access token');
        }

        $this->loadFromDB(null, null, 'G'.$user_data['sub']);

        if ($this->getId() === null) {
            $username = $user_data['given_name'];
            $this->name = $username;
            $this->foreign_id = 'G'.$user_data['sub'];
            $this->passhash = '!';
            $this->saveToDB();
        }
    }

    /**
     * @throws Exception
     */
    public function loadFromDB(?int $id = null, ?string $name = null, ?string $foreign_id = null): bool
    {
        if ($id !== null) {
            $row = DB::getInstance()->getrow(
                "SELECT u_id, u_name, u_pass
                FROM `user`
                WHERE u_id=?",
                ['i', $id]
            );
        } elseif ($name !== null) {
            $row = DB::getInstance()->getrow(
                "SELECT u_id, u_name, u_pass
                FROM `user`
                WHERE u_name=?",
                ['s', $name]
            );
        } else {
            $row = DB::getInstance()->getrow(
                "SELECT u_id, u_name, u_pass
                FROM `user`
                WHERE u_foreign_id=?",
                ['s', $foreign_id]
            );
        }

        $this->id = null;

        if ($row !== null) {
            $this->id = $row['u_id'];
            $this->name = $row['u_name'];
            $this->passhash = $row['u_pass'];

            return true;
        }

        return false;
    }

    /**
     * @throws Exception
     */
    private function saveToDB(): void
    {
        $db = DB::getInstance();

        if ($this->id === null) {
            $db->q(
                "INSERT INTO `user`
                SET u_name=?, u_pass=?, u_foreign_id=?",
                ['sss', $this->name, $this->passhash, $this->foreign_id]
            );
            $this->id = $db->insert_id;
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function verifyPassword(string $password): bool
    {
        return sha1($this->name . ':game:' . $password) === $this->passhash;
    }

    public function getSocket(): ?Connection
    {
        return $this->socket;
    }

    public function getGameId(): ?int
    {
        return $this->game_id;
    }

    public function setGameId(?int $game_id): void
    {
        $this->game_id = $game_id;
    }

    public function send(string $msg): void
    {
        $this->socket->send($msg);
    }

    public function __toString(): string
    {
        return sprintf("User: [id: %d, name: %s, socketId: %d, gameId: %s]\n",
            $this->id,
            $this->name,
            $this->getSocket()->getId(),
            $this->game_id ?? '-'
        );
    }
}
