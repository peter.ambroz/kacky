<?php

declare(strict_types=1);

namespace App\Kacky;

use App\GameManager\GameServer;
use App\GameManager\Message;
use App\GameManager\ObjectWithId;
use App\Kacky\Enum\CardClass;
use App\Kacky\Enum\CardType;
use App\Kacky\Enum\Color;
use App\Kacky\Enum\DuckState;
use Exception;

class Game
{
    private const CMD_TEXT = 100;
    private const CMD_ACTION = 101;
    private const CMD_UNPROTECT = 102;

    public const INACTIVE = 0;
    public const ACTIVE = 1;
    public const OVER = 2;

    public const P_MIN = 2;
    public const P_MAX = 6;

    private ?int $id = null;

    private int $status = self::INACTIVE;

    private int $timestamp;

    /** @var Player[] */
    private array $players = [];

    private Table $table;

    private int $activePlayer = 0;

    /** @var string[] */
    private array $chatMessages = [];

    function __construct(private readonly string $title)
    {
        $this->timestamp = time();
    }

    /**
     * @throws Exception
     */
    public function start(): void
    {
        if (count($this->players) < Game::P_MIN || count($this->players) > Game::P_MAX) {
            throw new Exception('Incorrect number of players');
        }

        foreach ($this->players as $player) {
            $player->reset();
        }

        $this->table = new Table($this->players);

        shuffle($this->players);
        $this->activePlayer = 0;
        $this->status = self::ACTIVE;
    }

    private function getPlayerIdByUserId(int $user_id): int|false
    {
        foreach ($this->players as $id => $player) {
            if ($user_id === $player->getId()) {
                return $id;
            }
        }

        return false;
    }

    private function getPlayerNameByRiverPos(int $pos): string
    {
        $duck = $this->table->getDuckOnBoard($pos);

        if ($duck->getProtectionStatus() === DuckState::DUCK_ON_DUCK) {
            $color = $duck->getProtectingDuck()->getColor();
        } else {
            $color = $duck->getColor();
        }

        if ($color === Color::WATER) {
            return 'voda';
        }

        foreach ($this->players as $player) {
            if ($color === $player->getColor()) {
                return $player->getName();
            }
        }

        return '';
    }

    private function getPossibleMoves(int $playerId): array
    {
        $player = $this->players[$playerId];

        $master = [];
        $moves = [];
        $canMove = false;

        foreach ($this->table->getHand($playerId) as $k => $cardInHand) {
            $moves[$k] = [];

            for ($i = 0; $i < Table::VISIBLE_DUCKS; $i++) {
                $request = new ValidateRequest($player->getColor(), $cardInHand, $i, null);
                $canMove |= ($val = $cardInHand->validate($this->table, $request));
                $moves[$k][$i] = $val;
            }
            $moves[$k][Table::PILE] = false;
        }

        if (!$canMove) {
            foreach ($moves as $k => $v) {
                $moves[$k][Table::PILE] = true;
            }
        }

        $master[0] = $moves;

        $moves = [];

        foreach ($this->table->getHand($playerId) as $k => $cardInHand) {
            switch ($cardInHand->getClass()) {
                case CardClass::DOUBLE: // JEJDA_VEDLE, ZIVY_STIT
                    $moves[$k] = [];

                    for ($i = 0; $i < Table::VISIBLE_DUCKS; $i++) {
                        $moves[$k][$i] = [];

                        for ($j = 0; $j < Table::VISIBLE_DUCKS; $j++) {
                            $request = new ValidateRequest($player->getColor(), $cardInHand, $i, $j);
                            $val = $cardInHand->validate($this->table, $request);
                            $moves[$k][$i][$j] = $val;
                        }
                    }
                    break;

                case CardClass::SPECIAL: // ROSAMBO
                    $moves[$k] = true;
                    break;

                case CardClass::SINGLE:
                    // CHVATAM, LEHARO, ZAMIRIT, VYSTRELIT, DVOJITA_HROZBA, DVOJITA_TREFA, STRILEJ_VPRAVO, STRILEJ_VLEVO, DIVOKEJ_BILL, KACHNI_UNIK, TURBOKACHNA
                case CardClass::ZERO:
                    // KACHNI_POCHOD, KACHNI_TANEC
                    $moves[$k] = false;
                    break;
            }
        }

        $master[1] = $moves;

        return $master;
    }

    private function useCard(int $playerId, int $cardInHandId): void
    {
        $this->table->discard($this->table->removeCard($playerId, $cardInHandId));
        $this->table->addCard($playerId);
    }

    private function isPlayable(PlayRequest $request): bool
    {
        $moves = $this->getPossibleMoves($request->playerId);

        return $moves[0][$request->cardPos][$request->duck0];
    }

    /**
     * @throws Exception
     */
    public function play(PlayRequest $request): array|false
    {
        $playerId = $request->playerId;
        $player = $this->players[$playerId];
        $cardInHand = $this->table->getCard($playerId, $request->cardPos);

        if ($this->activePlayer !== $playerId || !$this->isPlayable($request)) {
            return false;
        }

        $response = new PlayResponse();

        if ($request->duck0 === Table::PILE) {
            $this->useCard($playerId, $request->cardPos);
            $playMessage = sprintf('%s zahadzuje %s', $player->getName(), mb_strtoupper($cardInHand->getName(), 'UTF-8'));
        } else {
            $response = $cardInHand->play($this->table, $request);

            foreach ($this->players as $candidatePlayer) {
                foreach ($response->killedDucks as $killedDuck) {
                    if ($candidatePlayer->getColor() === $this->table->getDuckOnBoard($killedDuck)->getColor()) {
                        $candidatePlayer->decreaseLives();
                    }
                }
            }

            // in all cases (except for KACHNI_UNIK), the player's card has been used by now
            if ($cardInHand->getType() !== CardType::KACHNI_UNIK) {
                $this->useCard($playerId, $request->cardPos);
            }

            $playMessage = sprintf('%s - %s',
                $player->getName(),
                mb_strtoupper($cardInHand->getName(), 'UTF-8'),
            );

            if (count($response->affectedDucks) > 0) {
                $playMessage .= sprintf(' na %s (%s)',
                    implode(', ', array_map(fn($duckId) => $duckId + 1, $response->affectedDucks)),
                    implode(', ', array_map(fn($duckId) => $this->getPlayerNameByRiverPos($duckId), $response->affectedDucks)),
                );
            }
        }

        $messages = [
            [
                'cmd' => self::CMD_TEXT,
                'text' => sprintf('<span class="msg-sys0">%s</span>', htmlentities($playMessage)),
            ], [
                'cmd' => self::CMD_ACTION,
                'player_id' => $playerId,
                'card_id' => $cardInHand->getType()->value,
                'river_pos' => $request->duck0,
                'extras' => $request->duck1,
                'died_pos' => $response->killedDucks,
                'self' => false,
            ],
        ];

        $this->activePlayer = ($this->activePlayer + 1) % count($this->players);

        // skontrolujeme, ci niekto vyhral
        $alive = 0;
        $winner = 'voda';

        foreach ($this->players as $player) {
            if ($player->getLives() > 0) {
                $alive++;
                $winner = $player->getName();
            }
        }

        if ($alive <= 1) {
            $this->status = self::OVER;
            $this->activePlayer = -1;

            // game-over text message
            $messages[] = [
                'cmd' => self::CMD_TEXT,
                'text' => sprintf('<span class="msg-sys0">Game over! %s je víťaz! <a href="javascript:game_start()">*Hrať znova*</a></span>', mb_strtoupper($winner, 'UTF-8'))
            ];
        }

        $unprotected = $this->table->unprotectDucks();

        if (count($unprotected) > 0) {
            $messages[] = [
                'cmd' => self::CMD_UNPROTECT,
                'positions' => $unprotected
            ];
        }

        return $messages;
    }

    public function getDetails(int $playerId): array
    {
        $state = [];

        $state['title'] = $this->title;
        $state['active'] = $this->status;
        $state['players'] = [];

        foreach ($this->players as $k => $player) {
            $state['players'][$k] = [
                'id' => $player->getId(),
                'name' => $player->getName(),
                'lives' => $player->getLives(),
                'color' => $player->getColor()->value,
                'current' => ($k === $playerId),
                'on_move' => ($k === $this->activePlayer),
            ];
        }

        if ($this->status === self::INACTIVE) {
            return $state;
        }

        $state['river'] = $this->table->getRiverDetails();

        $state['pile'] = [
            'id' => false,
            'name' => false,
            'desc' => false,
        ];

        /** @var ActionCard $pileTop */
        $pileTop = $this->table->getTrashTop();

        if ($pileTop !== null) {
            $state['pile'] = [
                'id' => $pileTop->getType()->value,
                'name' => $pileTop->getName(),
                'desc' => $pileTop->getDescription(),
            ];
        }

        $state['hand'] = [];
        foreach ($this->table->getHand($playerId) as $card) {
            $state['hand'][] = [
                'id' => $card->getType()->value,
                'name' => $card->getName(),
                'desc' => $card->getDescription(),
            ];
        }

        $state['moves'] = $this->getPossibleMoves($playerId);

        return $state;
    }

    /**
     * @throws Exception
     */
    public function processMessage(ObjectWithId $user, string $cmd, array $args, GameServer $ms): void
    {
        // bump game timestamp
        $this->timestamp = time();

        switch ($cmd) {
            case 'setColor':
                if (!array_key_exists('color', $args)) {
                    throw new Exception('Color required');
                }

                $wantColor = Color::tryFrom((int) $args['color']);

                if ($wantColor === null) {
                    throw new Exception('Color invalid');
                }

                // check if the color is available
                if ($wantColor !== Color::WATER) {
                    foreach ($this->players as $waitingUser) {
                        if ($waitingUser->getColor() === $wantColor) {
                            throw new Exception('Color in use');
                        }
                    }
                }

                $player_id = $this->getPlayerIdByUserId($user->getId());

                if ($wantColor === Color::WATER) {
                    $wantColor = null;
                }
                $this->players[$player_id]->setColor($wantColor);

                $ms->sendMany($this->players, (string) new Message('setColor', ['userId' => $user->getId(), 'color' => $wantColor->value]));
                break;

            case 'gameDetails':
                $player_id = $this->getPlayerIdByUserId($user->getId());

                $ms->send($user, (string) new Message('gameDetails', $this->getDetails($player_id)));
                break;

            case 'gameStart':
                $this->start();
                $player_id = $this->getPlayerIdByUserId($user->getId());

                if ($player_id === false) {
                    throw new Exception('Invalid user id');
                }

                $ms->sendMany($this->players, (string) new Message('gameStart'));

                foreach($this->players as $player_num => $player) {
                    $ms->send($player, (string) new Message('gameDetails', $this->getDetails($player_num)));
                }

                break;

            case 'cardPlay':
                $player_id = $this->getPlayerIdByUserId($user->getId());

                if ($player_id === false) {
                    throw new Exception('Invalid user id');
                }

                if ($this->status === self::INACTIVE) {
                    throw new Exception('Inactive game');
                }

                if (strlen($args['param2'])) {
                    $args['param2'] = array_map(fn ($v) => (int) $v, explode(' ', trim($args['param2'])));
                } else {
                    $args['param2'] = null;
                }

                if ($args['param1'] !== null) {
                    $args['param1'] = (int) $args['param1'];
                }

                $request = new PlayRequest($player_id, (int) $args['card_id'], (int) $args['param0'], $args['param1'], $args['param2']);
                $ret = $this->play($request);

                if ($ret === false) {
                    throw new Exception('Game::play() returned false');
                }

                // put new chat messages into archive
                foreach ($ret as $message) {
                    if ($message['cmd'] === self::CMD_TEXT) {
                        $this->chatMessages[] = $message['text'];
                    }
                }

                foreach ($this->players as $player_num => $player) {
                    $gstate = $this->getDetails($player_num);
                    $gstate['messages'] = $ret;

                    // mark the self-generated gameplay messages
                    if ($player_num == $player_id) {
                        foreach ($gstate['messages'] as &$message) {
                            if ($message['cmd'] === self::CMD_ACTION) {
                                $message['self'] = true;
                            }
                        }

                        unset($message);
                    }

                    $ms->send($player, (string) new Message('gameDetails', $gstate));
                }

                break;

            case 'chatLoad': // load all previous chat messages
                $ms->send($user, (string) new Message('chatLoad', $this->chatMessages));
                break;

            case 'chat':
                if (!strlen($args['text'] ?? '')) {
                    throw new Exception('Empty message');
                }

                if ($this->status === self::INACTIVE) {
                    throw new Exception('Game not started');
                }

                $player = $this->players[$this->getPlayerIdByUserId($user->getId())];
                $msg = mb_substr($args['text'], 0, 512, 'UTF-8');
                $wholeMsg = sprintf('<span class="msg-col%d">%s: %s</span>',
                    $player->getColor()->value,
                    htmlentities($player->getName(), ENT_COMPAT | ENT_HTML5, 'UTF-8'),
                    htmlentities($msg, ENT_COMPAT | ENT_HTML5, 'UTF-8')
                );
                $this->chatMessages[] = $wholeMsg;
                $ms->sendMany($this->players, (string) new Message('chat', ['text' => $wholeMsg]));
                break;

            default:
                throw new Exception('Unhandled message in App\Kacky\Game');
        }
    }

    /**
     * @return Player[]
     */
    public function getWaitingUsers(): array
    {
        return $this->players;
    }

    public function hasPlayerById(int $userId): bool
    {
        $playerId = $this->getPlayerIdByUserId($userId);

        return !($playerId === false);
    }

    public function addWaitingUser(int $userId, string $name): void
    {
        $this->players[] = new Player($userId, $name);
    }

    public function removeWaitingUser(int $userId): void
    {
        $playerId = $this->getPlayerIdByUserId($userId);

        if ($playerId !== false) {
            array_splice($this->players, $playerId, 1);
        }
    }

    public function connectionStatusChange(int $user_id, int $status, GameServer $ms): void
    {
        $statusMap = [
            0 => 'DISCONNECT: %s je OFFLINE',
            1 => 'CONNECT: %s je ONLINE'
        ];
        $player_id = $this->getPlayerIdByUserId($user_id);

        if ($player_id !== false) {
            $msg = sprintf('<span class="msg-sys0">'.$statusMap[$status].'</span>',
                $this->players[$player_id]->getName()
            );
            $this->chatMessages[] = $msg;
            $ms->sendMany($this->players, (string) new Message('chat', ['text' => $msg]));
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function __toString(): string
    {
        return sprintf("Game: [id: %d, title: %s, status: %d, activePlayer: %d, players:\n%s]\n",
            $this->id,
            $this->title,
            $this->status,
            $this->activePlayer,
            implode('', $this->players)
        );
    }

    public function getAge(): int
    {
        return time() - $this->timestamp;
    }
}
