<?php

declare(strict_types=1);

namespace App\Kacky;

use App\Kacky\Enum\CardType;
use App\Kacky\Enum\Color;
use App\Kacky\Enum\DuckState;

class Table
{
    public const VISIBLE_DUCKS = 6;
    public const PILE = self::VISIBLE_DUCKS;

    private  const HAND_SIZE = 3;
    private const WATER_CARDS = 5;

    /** @var Duck[] */
    private array $ducksOnBoard;

    private Stack $ducksInDeck;

    /** @var bool[] */
    private array $targeted;

    private Stack $cardTrash;

    private Stack $cardPile;

    /** @var ActionCard[][] */
    private array $hands;

    /**
     * @param Player[] $players
     */
    function __construct(array $players)
    {
        $this->cardPile = new Stack();
        $this->cardTrash = new Stack();
        $this->ducksInDeck = new Stack();
        $this->ducksOnBoard = [];
        $this->targeted = [];

        // add colored ducks into the deck
        foreach ($players as $player) {
            for ($i = 0; $i < Player::LIVES; $i++) {
                $this->ducksInDeck->add(new Duck($player->getColor()));
            }
        }

        // add water to the deck
        for ($i = 0; $i < self::WATER_CARDS; $i++) {
            $this->ducksInDeck->add(new Water());
        }

        // set default targets to false
        for ($i = 0; $i < self::VISIBLE_DUCKS; $i++) {
            $this->targeted[$i] = false;
        }

        foreach (CardType::cases() as $case) {
            $cardCount = match ($case) {
                CardType::DIVOKEJ_BILL,
                CardType::DVOJITA_HROZBA,
                CardType::DVOJITA_TREFA,
                CardType::KACHNI_TANEC,
                CardType::TURBOKACHNA => 1,

                CardType::ZIVY_STIT,
                CardType::ROSAMBO,
                CardType::STRILEJ_VLEVO,
                CardType::STRILEJ_VPRAVO,
                CardType::JEJDA_VEDLE => 2,

                CardType::KACHNI_UNIK,
                CardType::LEHARO,
                CardType::CHVATAM => 3,

                CardType::KACHNI_POCHOD => 6,

                CardType::ZAMIRIT => 10,

                CardType::VYSTRELIT => 12,
            };

            for ($i = 0; $i < $cardCount; $i++) {
                $this->cardPile->push(ActionFactory::create($case));
            }
        }

        $this->cardPile->shuffle();
        $this->ducksInDeck->shuffle();

        $this->refillDucksOnBoard();

        foreach ($players as $pid => $player) {
            $this->hands[$pid] = [];

            for ($i = 0; $i < self::HAND_SIZE; $i++) {
                $this->addCard($pid);
            }
        }
    }

    public function addCard(int $playerId): void
    {
        $this->hands[$playerId][] = $this->drawCardPile();
    }

    public function getCard(int $playerId, int $index): ActionCard
    {
        return $this->hands[$playerId][$index];
    }

    public function removeCard(int $playerId, int $index): ActionCard
    {
        return array_splice($this->hands[$playerId], $index, 1)[0];
    }

    /**
     * @return ActionCard[]
     */
    public function getHand(int $playerId): array
    {
        return $this->hands[$playerId];
    }

    public function getDuckOnBoard(int $index): Duck
    {
        return $this->ducksOnBoard[$index];
    }

    /**
     * @return int[]
     */
    public function unprotectDucks(): array
    {
        $unprotected = [];

        foreach ($this->ducksOnBoard as $k => $duck) {
            $card = $duck->decreaseProtection();

            if ($card !== null) {
                $this->cardTrash->push($card);
                $unprotected[] = $k;
            }
        }

        return $unprotected;
    }

    public function refillDucksOnBoard(): void
    {
        while (count($this->ducksOnBoard) < self::VISIBLE_DUCKS) {
            /** @var Duck|null $deckDuck */
            $deckDuck = $this->ducksInDeck->get();
            $this->ducksOnBoard[] = $deckDuck ?? new Water();
        }
    }

    public function discard(ActionCard $card): void
    {
        $this->cardTrash->push($card);
    }

    public function getTrashTop(): ?ActionCard
    {
        return $this->cardTrash->peek();
    }

    public function isTargeted(int $idx): bool
    {
        return $this->targeted[$idx];
    }

    public function setTarget(int $idx): void
    {
        $this->targeted[$idx] = true;
    }

    public function resetTarget(int $index): void
    {
        $this->targeted[$index] = false;
    }

    /**
     * Removes the duck from the board and either places it again onto the deck or discards.
     */
    public function removeDuck(int $idx, bool $kill): ?Color
    {
        $duckOnBoard = $this->ducksOnBoard[$idx];
        $protectionStatus = $duckOnBoard->getProtectionStatus();

        if ($kill) {
            switch ($protectionStatus) {
                case DuckState::DUCK_ONLY:
                    array_splice($this->ducksOnBoard, $idx, 1);

                    return $duckOnBoard->getColor();

                case DuckState::DUCK_PROTECTED:
                    return Color::WATER;

                case DuckState::DUCK_ON_DUCK:
                    return $duckOnBoard->removeDuckProtection()->getColor();

                case DuckState::DUCK_ON_DUCK_PROTECTED:
                    $this->ducksOnBoard[$idx] = $duckOnBoard->removeDuckProtection();

                    return $duckOnBoard->getColor();
            }
        } else {
            if ($protectionStatus === DuckState::DUCK_PROTECTED) {
                $this->cardTrash->push($duckOnBoard->removeActionProtection());
            } elseif ($protectionStatus === DuckState::DUCK_ON_DUCK) {
                $this->ducksInDeck->add($duckOnBoard->removeDuckProtection());
            } elseif ($protectionStatus === DuckState::DUCK_ON_DUCK_PROTECTED) {
                $this->cardTrash->push($duckOnBoard->removeActionProtection());
                $this->ducksInDeck->add($duckOnBoard->removeDuckProtection());
            }

            array_splice($this->ducksOnBoard, $idx, 1);
            $this->ducksInDeck->add($duckOnBoard);
        }

        return null;
    }

    public function swapDucks(int $idx1, int $idx2): void
    {
        $temp = $this->ducksOnBoard[$idx1];
        $this->ducksOnBoard[$idx1] = $this->ducksOnBoard[$idx2];
        $this->ducksOnBoard[$idx2] = $temp;
    }

    public function reorderDucks(array $permutation): void
    {
        $reorderedDucks = [];

        foreach ($permutation as $val) {
            $reorderedDucks[] = $this->ducksOnBoard[$val];
        }

        $this->ducksOnBoard = $reorderedDucks;
    }

    public function reshuffleDucks(): void
    {
        for ($i = self::VISIBLE_DUCKS - 1; $i >= 0; $i--) {
            $this->removeDuck($i, false);
        }

        $this->ducksInDeck->shuffle();
        $this->refillDucksOnBoard();
    }

    public function protectDuckWithAction(Duck $duck, ActionCard $card): void
    {
        if (in_array($duck->getProtectionStatus(), [DuckState::DUCK_ONLY, DuckState::DUCK_ON_DUCK], true)) {
            $duck->protectByActionCard($card, count($this->hands));
        }
    }

    public function protectDuckWithDuck(int $idx, int $protectorIdx): void
    {
        $duck = $this->ducksOnBoard[$idx];
        $protector = $this->ducksOnBoard[$protectorIdx];

        if ($duck->getProtectionStatus() === DuckState::DUCK_ONLY) {
            $duck->protectByDuck($protector);
            array_splice($this->ducksOnBoard, $protectorIdx, 1);
            $this->refillDucksOnBoard();
        }
    }

    public function getRiverDetails(): array
    {
        $river = [];

        foreach ($this->ducksOnBoard as $k => $duck) {
            $features = $duck->getProtectionStatus();
            $protectingDuck = $duck->getProtectingDuck();
            $protectingAction = $duck->getProtectingAction();

            $tmp = [
                'color' => $duck->getColor()->value,
                'target' => $this->isTargeted($k),
                'action_card' => false,
                'other_duck' => false,
                'features' => $features->value,
            ];

            switch ($features) {
                case DuckState::DUCK_PROTECTED :
                    $tmp['action_card'] = $protectingAction->getType()->value;
                    break;
                case DuckState::DUCK_ON_DUCK :
                    $tmp['other_duck'] = $protectingDuck->getColor()->value;
                    break;
                case DuckState::DUCK_ON_DUCK_PROTECTED :
                    $tmp['action_card'] = $protectingAction->getType()->value;
                    $tmp['other_duck'] = $protectingDuck->getColor()->value;
                    break;
                default:
                    break;
            }

            $river[] = $tmp;
        }

        return $river;
    }

    public function isOwn(Color $playerColor, int $index): bool
    {
        $card = $this->ducksOnBoard[$index];

        return ($playerColor === $card->getColor() || $playerColor === $card->getProtectingDuck()?->getColor());
    }

    private function drawCardPile(): ActionCard
    {
        $card = $this->cardPile->pop();

        if ($card === null) {
            $this->cardPile->setItems($this->cardTrash->getItems());
            $this->cardTrash = new Stack();
            $this->cardPile->shuffle();
            $card = $this->cardPile->pop();
        }

        return $card;
    }
}
