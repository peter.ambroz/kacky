<?php

declare(strict_types=1);

namespace App\Kacky;

use App\Kacky\Enum\Color;

class ValidateRequest
{
    public function __construct(
        public readonly Color $myColor,
        public readonly ActionCard $actionCard,
        public readonly int $duck0,
        public readonly ?int $duck1,
    ) {}
}
