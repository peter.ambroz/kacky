<?php

declare(strict_types=1);

namespace App\Kacky;

use App\Kacky\Action\Chvatam;
use App\Kacky\Action\DivokejBill;
use App\Kacky\Action\DvojitaHrozba;
use App\Kacky\Action\DvojitaTrefa;
use App\Kacky\Action\JejdaVedle;
use App\Kacky\Action\KachniPochod;
use App\Kacky\Action\KachniTanec;
use App\Kacky\Action\KachniUnik;
use App\Kacky\Action\Leharo;
use App\Kacky\Action\Rosambo;
use App\Kacky\Action\StrilejVlevo;
use App\Kacky\Action\StrilejVpravo;
use App\Kacky\Action\Turbokachna;
use App\Kacky\Action\Vystrelit;
use App\Kacky\Action\Zamirit;
use App\Kacky\Action\ZivyStit;
use App\Kacky\Enum\CardType;

class ActionFactory
{
    public static function create(CardType $type): ActionCard
    {
        return match ($type) {
            CardType::DIVOKEJ_BILL => new DivokejBill(),
            CardType::DVOJITA_HROZBA => new DvojitaHrozba(),
            CardType::DVOJITA_TREFA => new DvojitaTrefa(),
            CardType::KACHNI_TANEC => new KachniTanec(),
            CardType::TURBOKACHNA => new Turbokachna(),
            CardType::ZIVY_STIT => new ZivyStit(),
            CardType::ROSAMBO => new Rosambo(),
            CardType::STRILEJ_VLEVO => new StrilejVlevo(),
            CardType::STRILEJ_VPRAVO => new StrilejVpravo(),
            CardType::JEJDA_VEDLE => new JejdaVedle(),
            CardType::KACHNI_UNIK => new KachniUnik(),
            CardType::LEHARO => new Leharo(),
            CardType::CHVATAM => new Chvatam(),
            CardType::KACHNI_POCHOD => new KachniPochod(),
            CardType::ZAMIRIT => new Zamirit(),
            CardType::VYSTRELIT => new Vystrelit(),
        };
    }
}
