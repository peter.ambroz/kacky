<?php

declare(strict_types=1);

namespace App\Kacky\Enum;

enum DuckState: int
{
    case DUCK_ONLY = 0;
    case DUCK_PROTECTED = 1;
    case DUCK_ON_DUCK = 2;
    case DUCK_ON_DUCK_PROTECTED = 3;
}
