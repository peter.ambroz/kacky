<?php

declare(strict_types=1);

namespace App\Kacky\Enum;

enum CardClass
{
    case ZERO; // Da sa zahrat vzdy, nepotrebuje cielovu kartu
    case SINGLE; // Vyzaduje 1 cielovu kartu
    case DOUBLE; // Vyzaduje 1 cielovu a 1 pomocnu kartu
    case SPECIAL; // Specialna karta s vyssim poctom parametrov (ROSAMBO)
}
