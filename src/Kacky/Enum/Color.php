<?php

declare(strict_types=1);

namespace App\Kacky\Enum;

enum Color: int
{
    case WATER = - 1;
    case VIOLET = 0;
    case GREEN = 1;
    case BLUE = 2;
    case ORANGE = 3;
    case YELLOW = 4;
    case PINK = 5;
}
