<?php

declare(strict_types=1);

namespace App\Kacky\Enum;

enum CardType: int
{
    case DIVOKEJ_BILL = 0;
    case DVOJITA_HROZBA = 1;
    case DVOJITA_TREFA = 2;
    case KACHNI_TANEC = 3;
    case TURBOKACHNA = 4;
    case ZIVY_STIT = 5;
    case ROSAMBO = 6;
    case STRILEJ_VLEVO = 7;
    case STRILEJ_VPRAVO = 8;
    case JEJDA_VEDLE = 9;
    case KACHNI_UNIK = 10;
    case LEHARO = 11;
    case CHVATAM = 12;
    case KACHNI_POCHOD = 13;
    case ZAMIRIT = 14;
    case VYSTRELIT = 15;
}
