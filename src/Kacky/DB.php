<?php

namespace App\Kacky;

use Exception;
use PDO;

class DB
{
    private static ?MySQL $db = null;

    /**
     * @throws Exception
     */
    public static function getConfig(?string $configFile = null): array
    {
        $configFile = $configFile ?? __DIR__.'/../../config/DB.json';

        if (!file_exists($configFile)) {
            throw new Exception('DB config file not found: ' . $configFile);
        }

        return json_decode(file_get_contents($configFile), true);
    }

    /**
     * @throws Exception
     */
    public static function getDSN(?array $config = null): string
    {
        $config = $config ?? static::getConfig();
        return 'mysql:host='.$config['host'].';dbname='.$config['db'];
    }

    /**
     * @throws Exception
     */
    public static function getPDO(?array $config = null): PDO
    {
        $config = $config ?? static::getConfig();
        $pdo = new PDO(static::getDSN($config), $config['user'], $config['pass']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }

    /**
     * @throws Exception
     */
    public static function getInstance(): MySQL
    {
        if (!is_null(static::$db) && !(static::$db->ping())) {
            static::$db = null;
        }

        if (is_null(static::$db)) {
            $dbConfig = static::getConfig();
            static::$db = new MySQL($dbConfig['host'], $dbConfig['user'], $dbConfig['pass'], $dbConfig['db']);
        }

        return static::$db;
    }

    private function __construct() {}
    private function __clone () {}
}
