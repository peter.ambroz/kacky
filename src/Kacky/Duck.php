<?php

declare(strict_types=1);

namespace App\Kacky;

use App\Kacky\Enum\Color;
use App\Kacky\Enum\DuckState;

class Duck
{
    private int $protectionTtl = 0;

    private ?ActionCard $actionProtection = null;

    private ?Duck $duckProtection = null;

    function __construct(private readonly Color $color)
    {
    }

    public function getColor(): Color
    {
        return $this->color;
    }

    public function protectByActionCard(ActionCard $card, int $ttl): void
    {
        if ($this->duckProtection !== null) {
            $this->duckProtection->protectByActionCard($card, $ttl);
        } else {
            $this->actionProtection = $card;
            $this->protectionTtl = $ttl;
        }
    }

    public function protectByDuck(Duck $card): void
    {
        $this->duckProtection = $card;
        $this->protectionTtl = 0;
    }

    public function decreaseProtection(): ?ActionCard
    {
        if ($this->actionProtection !== null) {
            $this->protectionTtl--;

            if ($this->protectionTtl <= 0) {
                return $this->removeActionProtection();
            }

            return null;
        }

        return $this->duckProtection?->decreaseProtection();
    }

    public function getProtectingDuck(): ?Duck
    {
        return $this->duckProtection;
    }

    public function getProtectingAction(): ?ActionCard
    {
        return $this->actionProtection;
    }

    public function getProtectionStatus(): DuckState
    {
        if ($this->actionProtection === null && $this->duckProtection === null) {
            return DuckState::DUCK_ONLY;
        }

        if ($this->actionProtection !== null) {
            return DuckState::DUCK_PROTECTED;
        }

        return match ($this->duckProtection->getProtectionStatus()) {
            DuckState::DUCK_ONLY => DuckState::DUCK_ON_DUCK,
            DuckState::DUCK_PROTECTED => DuckState::DUCK_ON_DUCK_PROTECTED,
        };
    }

    public function __toString(): string
    {
        return sprintf("Duck: [color: %d, ttl: %d, actionCard: \n%s\nduckCard: \n%s]\n",
            $this->color,
            $this->protectionTtl,
            $this->actionProtection ?? '-',
            $this->duckProtection ?? '-'
        );
    }

    public function removeActionProtection(): ?ActionCard
    {
        if ($this->duckProtection !== null) {
            return $this->duckProtection->removeActionProtection();
        }

        $temp = $this->actionProtection;
        $this->actionProtection = null;
        $this->protectionTtl = 0;

        return $temp;
    }

    public function removeDuckProtection(): ?Duck
    {
        $temp = $this->duckProtection;
        $this->duckProtection = null;

        return $temp;
    }
}
