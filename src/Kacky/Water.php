<?php

declare(strict_types=1);

namespace App\Kacky;

use App\Kacky\Enum\Color;

class Water extends Duck
{
    public function __construct()
    {
        parent::__construct(Color::WATER);
    }
}
