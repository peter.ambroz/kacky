<?php

declare(strict_types=1);

namespace App\GameManager;

interface ObjectWithId
{
    public function getId(): ?int;
}
