<?php

namespace App\GameManager;

use App\Kacky\DB;
use GuzzleHttp\Psr7\Header;
use Ratchet\ConnectionInterface;
use Ratchet\Session\Serialize\PhpHandler;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

/**
 * Class Connection
 * Wraps the ConnectionInterface which unfortunately uses magic methods and decorations.
 * Implements the getters for values that are not a part of the official interface.
 */
class Connection implements ConnectionInterface
{
    private ConnectionInterface $connectionInterface;

    public function __construct(ConnectionInterface $connectionInterface)
    {
        $this->connectionInterface = $connectionInterface;
    }

    function send($data): void
    {
        $this->connectionInterface->send($data);
    }

    function close(): void
    {
        $this->connectionInterface->close();
    }

    public function getId(): int
    {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->connectionInterface->resourceId;
    }

    public function getSession(array $config): array
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $cookies = $this->connectionInterface->httpRequest->getHeader('Cookie');
        if (count($cookies)) {
            $cookies = Header::parse($cookies)[0];
            if (array_key_exists($config['session_name'], $cookies)) {
                $session_id = $cookies[$config['session_name']];
            } else {
                return [];
            }
        } else {
            return [];
        }

        $handler = new PdoSessionHandler(DB::getPDO($config), ['db_table' => $config['session_table']]);
        $handler->open('', $config['session_name']);
        $rawData = $handler->read($session_id);
        $handler->close();

        $handler = new PhpHandler();
        $unserialized = $handler->unserialize($rawData);
        return $unserialized['_sf2_attributes'] ?? [];
    }
}
