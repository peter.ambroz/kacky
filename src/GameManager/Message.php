<?php

declare(strict_types=1);

namespace App\GameManager;

use Exception;

class Message
{
    public function __construct(private string $cmd, private array $args = [])
    {
    }

    /**
     * @throws Exception
     */
    public function decode(string $msg): void
    {
        // decode message
        $message = json_decode($msg, true);

        // validate message - must be JSON
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Not a JSON message');
        }

        // validate message - must be array with at least 'cmd' key
        if (!is_array($message)) {
            throw new Exception('Not an array');
        }
        if (!array_key_exists('cmd', $message)) {
            throw new Exception('No cmd key found');
        }

        $this->cmd = substr($message['cmd'], 0, 64);

        if (!array_key_exists('args', $message)) {
            $message['args'] = [];
        }

        if (!is_array($message['args'])) {
            $message['args'] = [$message['args']];
        }

        $this->args = $message['args'];
    }

    public function getCmd(): string
    {
        return $this->cmd;
    }

    public function getArgs(): array
    {
        return $this->args;
    }

    public static function error(string $error_msg): string
    {
        $msg = new static('error', ['text' => $error_msg]);

        return (string) $msg;
    }

    public static function ok(string $ok_msg): string
    {
        $msg = new static('ok', ['text' => $ok_msg]);

        return (string) $msg;
    }

    public function __toString(): string
    {
        return json_encode([
            'cmd' => $this->cmd,
            'args' => $this->args
        ]);
    }
}
