<?php

declare(strict_types=1);

namespace App\GameManager;

interface GameServer
{
    public function send(ObjectWithId $user, string $msg): void;

    /**
     * @param ObjectWithId[] $users
     */
    public function sendMany(array $users, string $msg): void;
}
