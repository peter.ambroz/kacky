<?php

declare(strict_types=1);

namespace App\GameManager;

use Exception;

class NotLoggedInException extends Exception
{
    public function __construct() {
        parent::__construct('Not logged in');
    }
}
