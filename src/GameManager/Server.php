<?php

declare(strict_types=1);

namespace App\GameManager;

use App\Kacky\DB;
use App\Kacky\Game;
use App\Kacky\Model\User;
use App\Kacky\Player;
use Exception;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class Server implements MessageComponentInterface, GameServer
{
    /** @var User[] */
    private array $userList;

    /** @var Game[] */
    private array $gameList;

    private array $config;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->userList = [];
        $this->gameList = [];
        $this->config = DB::getConfig();
    }

    /**
     * When a new connection is opened it will be passed to this method
     * @throws Exception
     */
    function onOpen(ConnectionInterface $conn): void
    {
        $conn = new Connection($conn);
        $this->userList[$conn->getId()] = new User($conn);
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).
     * SendMessage to $conn will not result in an error if it has already been closed.
     * @throws Exception
     */
    function onClose(ConnectionInterface $conn): void
    {
        $conn = new Connection($conn);
        $conn_id = $conn->getId();
        $user = $this->userList[$conn_id];
        $user_id = $user->getId();

        if ($user_id !== null) {
            foreach ($this->gameList as $game) {
                if ($game->hasPlayerById($user_id)) {
                    $game->connectionStatusChange($user_id, 0, $this);
                }
            }
        }

        unset($this->userList[$conn_id]);
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @throws Exception
     */
    function onError(ConnectionInterface $conn, Exception $e): void
    {
        $conn->close();
    }

    /**
     * Triggered when a client sends data through the socket
     * @param string $msg
     * @throws Exception
     */
    function onMessage(ConnectionInterface $from, $msg): void
    {
        try {
            $message = new Message('');
            $message->decode($msg);

            $from = new Connection($from);
            $resourceId = $from->getId();
            $this->processMessage($this->userList[$resourceId], $message->getCmd(), $message->getArgs());
        } catch (Exception $e) {
            $from->send(Message::error($e->getMessage()));
        }
    }

    private function isAuthenticated(User $user): bool
    {
        return $user->getId() !== null;
    }

    private function purgeOldGames(): void
    {
        // maximum allowed age in particular game activity states
        $ageMap = [
            Game::INACTIVE => 1800,
            Game::ACTIVE => 7200,
            Game::OVER => 7200,
        ];
        $oldGames = [];

        foreach ($this->gameList as $game_id => $game) {
            if ($game->getAge() > $ageMap[$game->getStatus()]) {
                $oldGames[] = $game_id;
            }
        }

        foreach ($oldGames as $game_id) {
            foreach ($this->userList as $user) {
                if ($user->getGameId() === $game_id) {
                    $user->setGameId(null);
                }
            }

            $this->sendMany($this->gameList[$game_id]->getWaitingUsers(), (string) new Message('gameClosed', ['gameId'=>$game_id]));
            unset($this->gameList[$game_id]);
        }
    }

    private function gameListing(User $user): array
    {
        $game_list = [];

        foreach ($this->gameList as $game_id => $game) {
            if ($game->getStatus() === Game::INACTIVE || $game->hasPlayerById($user->getId())) {
                $game_list[$game_id] = [
                    'title' => $game->getTitle(),
                    'players' => array_map(fn (Player $x) => $x->getName(), $game->getWaitingUsers()),
                    'active' => (($game->getStatus() !== Game::INACTIVE) ? ($game->getStatus() === Game::OVER ? 'ukončená' : 'prebieha') : 'pripravená'),
                ];
            }
        }

        return $game_list;
    }

    /**
     * @throws Exception
     */
    private function gameJoin(User $user, Game $game): bool
    {
        if ($game->getStatus() !== Game::INACTIVE && !$game->hasPlayerById($user->getId())) {
            throw new Exception('Game already started');
        }

        if (count($game->getWaitingUsers()) > Game::P_MAX) {
            throw new Exception('Too many players');
        }

        $user->setGameId($game->getId());

        if (!$game->hasPlayerById($user->getId())) {
            $game->addWaitingUser($user->getId(), $user->getName());

            return true;
        }

        return false;
    }

    /**
     * @throws Exception
     */
    private function processMessage(User $user, string $cmd, array $args): void
    {
        try {
            switch ($cmd) {
                case 'authenticate':
                    if (!array_key_exists('username', $args)) {
                        // try session authentication instead
                        $session = $user->getSocket()->getSession($this->config);
                        $logged = $session['isLogged'] ?? false;
                        $userId = $session['userId'] ?? 0;
                        if ($logged) {
                            $user->loadFromDB($userId);
                        } else {
                            throw new Exception('Session not authenticated');
                        }
                    } else {
                        // go for a traditional user/pass auth
                        $username = $args['username'] ?? '';
                        $password = $args['password'] ?? '';

                        $user->verifyFromDB($username, $password);
                    }

                    $preferredGameId = $args['gameId'] ?? 0;
                    if (array_key_exists($preferredGameId, $this->gameList)) {
                        $preferredGame = $this->gameList[$preferredGameId];

                        if ($preferredGame->hasPlayerById($user->getId())) {
                            $user->setGameId($preferredGameId);
                        }
                    }

                    // notify all concerned games about connected user
                    foreach($this->gameList as $game) {
                        if ($game->hasPlayerById($user->getId())) {
                            $game->connectionStatusChange($user->getId(), 1, $this);
                        }
                    }

                    $user->send(Message::ok('Authenticated'));

                    break;

                case 'gameList':
                    if (!$this->isAuthenticated($user)) {
                        throw new NotLoggedInException();
                    }

                    $this->purgeOldGames();
                    $user->send((string) new Message('gameList', $this->gameListing($user)));
                    break;

                case 'gameNew':
                    if (!$this->isAuthenticated($user)) {
                        throw new NotLoggedInException();
                    }

                    if (!array_key_exists('title', $args)) {
                        throw new Exception('Game title required');
                    }

                    $max_key = 0;
                    foreach ($this->gameList as $key => $value) {
                        if ($key > $max_key) {
                            $max_key = $key;
                        }
                    }

                    $new_id = $max_key + 1;
                    $new_game = new Game($args['title']);
                    $new_game->setId($new_id);
                    $this->gameList[$new_id] = $new_game;

                    $this->gameJoin($user, $new_game);

                    $user->send((string) new Message('gameNew', ['gameId' => $new_id]));
                    $this->sendNonPlaing(fn (User $x) => (string) new Message('gameList', $this->gameListing($x)));
                    break;

                case 'gameJoin':
                    if (!$this->isAuthenticated($user)) {
                        throw new NotLoggedInException();
                    }

                    if (!array_key_exists('gameId', $args)) {
                        throw new Exception('Game id required');
                    }

                    if (!array_key_exists($args['gameId'], $this->gameList)) {
                        throw new Exception('Game id invalid');
                    }

                    $joined_game = $this->gameList[$args['gameId']];
                    $ret = $this->gameJoin($user, $joined_game);

                    if ($ret) {
                        $this->sendMany($joined_game->getWaitingUsers(),
                            (string) new Message('gameJoin', ['userId' => $user->getId(), 'userName' => $user->getName()])
                        );
                    }
                    break;

                case 'gameLeave':
                    if (!$this->isAuthenticated($user)) {
                        throw new NotLoggedInException();
                    }

                    if ($user->getGameId() === null) {
                        throw new Exception('Not in game');
                    }

                    if (array_key_exists($user->getGameId(), $this->gameList)) {
                        $leaving_game = $this->gameList[$user->getGameId()];
                        if ($leaving_game->getStatus() !== Game::INACTIVE) {
                            throw new Exception('Game already started');
                        }

                        $leaving_game->removeWaitingUser($user->getId());
                        $this->sendMany($leaving_game->getWaitingUsers(),
                            (string) new Message('gameLeave', ['userId'=>$user->getId(), 'userName'=>$user->getName()])
                        );
                    }
                    $user->setGameId(null);
                    break;

                case 'debug':
                    echo "UserList: [\n".implode("", $this->userList)."]\n";
                    echo "GameList: [\n".implode("", $this->gameList)."]\n";
                    echo "\n";
                    break;

                // all other messages are forwarded to the game itself
                default:
                    if (!$this->isAuthenticated($user)) {
                        throw new NotLoggedInException();
                    }

                    if ($user->getGameId() === null) {
                        throw new Exception('Not in game');
                    }

                    $in_game = $this->gameList[$user->getGameId()];
                    $in_game->processMessage($user, $cmd, $args, $this);

                    break;
            }
        } catch (Exception $e) {
            $user->send(Message::error($e->getMessage()));
        }
    }

    public function send(ObjectWithId $user, string $msg): void
    {
        foreach ($this->userList as $connected_user) {
            if ($connected_user->getId() === $user->getId()) {
                $connected_user->send($msg);
            }
        }
    }

    /**
     * @param ObjectWithId[] $users
     */
    public function sendMany(array $users, string $msg): void
    {
        foreach ($users as $user) {
            $this->send($user, $msg);
        }
    }

    private function sendNonPlaing(?callable $msg_fn = null): void
    {
        foreach ($this->userList as $idle_user) {
            if ($idle_user->getGameId() === null) {
                $idle_user->send($msg_fn($idle_user));
            }
        }
    }
}
